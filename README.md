# README #

### What is this repository for? ###

* The start of a Space Trading game
* 0.00001

### How do I get set up? ###

* Install python 2.7, make sure you have the main.py and ship.py files in the same directory, type: python main.py
* Dependencies - Python 2.7, uses stdlib, nothing fancy

### Who do I talk to? ###

* Jeremy Kenyon - lanthos@gmail.com
