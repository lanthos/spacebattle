'''
Base shell just for testing attacking and defending logic
'''

import random
from ship import Ship

enemy = Ship()
player = Ship()

enemy.name = 'Evil Pirate'
player.name = 'Heroic Hero'
enemy.shields = True
done = False
while not done:
    choice = raw_input("There is an enemy here!  Would you like to attack? ")
    if choice.lower() in ["y", "yes"]:
        done = enemy.defend(player.attack())
        continue
