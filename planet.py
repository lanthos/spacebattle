'''

This will be the class for the planets.  The planets will need to have the following:

A tech level: 1 - 6.  Tech level will be used to determine what type of resources typically show up on the worlds as
well as what they require.  Higher tech levels means more refined products (advanced electronics, fancy foods, etc.).
Lower tech levels means that you will find more unrefined products (ores, basic foods, raw resources like sugar, etc).

Planet types: arid, desert, jungle, volcanic, arctic.  Different planet types will have specific resources that they
have more than others as well as specific types that they need more than others.

Planet population: high population means more people are buying and selling meaning that more trade happens at that
planet.

Law Level:  How corrupt are the police (if they have them)?  Will a bribe work?  Do they want more things that are
typically illegal somewhere else?

'''