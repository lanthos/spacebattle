import random


class Ship(object):
    '''
    SpaceShips:

    Each space ship should have the following:

    hull size, type of armor, # of weapon mounts, shield, # of engines

    upgrades: the following type of upgrades are available

    armor, shields, weapons, engines

    armor: is strong against projectile based weapons but weak against laser based weapons and missile weapons

    light armor, medium armor, heavy armor

    shields: are strong against laser based and missile based weapons but weak against projectile based weapons

    weapons: three weapon types.

    projectile, laser, missile

    '''
    def __init__(self):
        self.hull_size = 1
        self.max_hull_points = self.hull_size * 200  # The hit points for each ship are 200 * the size of the hull
        self.current_hull_points = self.max_hull_points
        self.armor_type = 0
        self.shields = False
        self.max_shield_points = self.hull_size * 200  # Shields effectively double hit points
        self.current_shield_points = self.max_shield_points
        self.weapon_mounts = 0
        self.number_of_engines = 1
        self.weapons = {"projectile": 1, "laser": 1, "missile": 1}
        self.max_cargo_space =

    def attack(self):
        '''
        Define attack parameters and return attack type and attack damage.  Damage types are projectile, missile, and
        laser.

        The projectile attack is good against shields as there is a lot of damage spread over a greater area.

        The missile attack is good against armor (explosive and heat ya know).

        The laser attack is good against armor as it can melt through it.
        '''
        while True:
            print("You have the following weapons installed: ")
            for types in self.weapons.items():
                print("{}: {}.".format(*types))
            choice = raw_input("Which weapon would you like to attack with? ")

            if choice.lower() in ["projectile", "p"]:
                choice = "projectile"
                if self.weapons[choice] == 0:
                    print("You don't have any of those installed!  Select another weapon type.")
                    continue
                else:
                    damage = 0
                    for each in range(self.weapons[choice]):
                        random.seed(None)
                        damage += random.randrange(20, 36)
                    attack_results = {choice: damage}
                return attack_results
            elif choice.lower() in ["laser", "l"]:
                choice = "laser"
                if self.weapons[choice] == 0:
                    print("You don't have any of those installed!  Select another weapon type.")
                    continue
                else:
                    damage = 0
                    for each in range(self.weapons[choice]):
                        random.seed(None)
                        damage += random.randrange(35, 51)
                    attack_results = {choice: damage}
                return attack_results
            elif choice.lower() in ["missile", "m"]:
                choice = "missile"
                if self.weapons[choice] == 0:
                    print("You don't have any of those installed!  Select another weapon type.")
                    continue
                else:
                    damage = 0
                    for each in range(self.weapons[choice]):
                        random.seed(None)
                        damage += random.randrange(50, 66)
                    attack_results = {choice: damage}
                return attack_results
            else:
                print("That's not a valid choice.")
                continue

    def defend(self, attack):
        '''
        Take the attack information and apply it to the current defense of the person being attacked.

        Projectiles do * 2 damage to shields and their damage is / 2 for hulls.

        Missiles do * 2 damage to hulls and / 3 damage to shields.

        Lasers do * 2 damage to hulls and / 3 damage to shields.

        Armor adds DR based upon type (0 = none, 1 = light (.20 DR), 2 = medium(.30 DR), 3 = heavy(.40 DR))
        '''
        for weapon in attack:
            if weapon == 'projectile':
                print("{} is getting attacked with a projectile weapon!".format(self.name))
                if not self.shields:
                    if self.armor_type == 0:
                        applied_damage = int(round(attack[weapon] / 2))
                        self.current_hull_points -= applied_damage
                    elif self.armor_type == 1:
                        applied_damage = int(round((attack[weapon] - (attack[weapon] * .20)) / 2))
                        self.current_hull_points -= applied_damage
                    elif self.armor_type == 2:
                        applied_damage = int(round((attack[weapon] - (attack[weapon] * .30)) / 2))
                        self.current_hull_points -= applied_damage
                    elif self.armor_type == 3:
                        applied_damage = int(round((attack[weapon] - (attack[weapon] * .40)) / 2))
                        self.current_hull_points -= applied_damage
                    if self.current_hull_points > 0:
                        print("{} took {} damage!  {}'s current hull integrity is at {}."
                              .format(self.name, applied_damage, self.name, self.current_hull_points))
                        return False
                    else:
                        print("{} took {} damage!  {}'s ship has been destroyed!"
                              .format(self.name, applied_damage, self.name))
                        return True
                else:
                    applied_damage = attack[weapon] * 2
                    self.current_shield_points -= applied_damage
                    if self.current_shield_points <= 0:
                        self.shields = False
                        print("{} took {} damage!  {}'s shield has been destroyed!"
                              .format(self.name, applied_damage, self.name))
                        return False
                    else:
                        print("{} took {} damage!  {}'s current shield strength is at {}."
                              .format(self.name, applied_damage, self.name, self.current_shield_points))
                        return False
            if weapon == 'missile':
                print("{} is getting attacked with a missile weapon!".format(self.name))
                if not self.shields:
                    if self.armor_type == 0:
                        applied_damage = attack[weapon] * 2
                        self.current_hull_points -= applied_damage
                    elif self.armor_type == 1:
                        applied_damage = int(round((attack[weapon] - (attack[weapon] * .20)) * 2))
                        self.current_hull_points -= applied_damage
                    elif self.armor_type == 2:
                        applied_damage = int(round((attack[weapon] - (attack[weapon] * .30)) * 2))
                        self.current_hull_points -= applied_damage
                    elif self.armor_type == 3:
                        applied_damage = int(round((attack[weapon] - (attack[weapon] * .40)) * 2))
                        self.current_hull_points -= applied_damage
                    if self.current_hull_points > 0:
                        print("{} took {} damage!  {}'s current hull integrity is at {}."
                              .format(self.name, applied_damage, self.name, self.current_hull_points))
                        return False
                    else:
                        print("{} took {} damage!  {}'s ship has been destroyed!"
                              .format(self.name, applied_damage, self.name))
                        return True
                else:
                    applied_damage = int(round(attack[weapon] / 3))
                    self.current_shield_points -= applied_damage
                    if self.current_shield_points <= 0:
                        self.shields = False
                        print("{} took {} damage!  {}'s shield has been destroyed!"
                              .format(self.name, applied_damage, self.name))
                        return False
                    else:
                        print("{} took {} damage!  {}'s current shield strength is at {}."
                              .format(self.name, applied_damage, self.name, self.current_shield_points))
                        return False
            if weapon == 'laser':
                print("{} is getting attacked with a laser weapon!".format(self.name))
                if not self.shields:
                    if self.armor_type == 0:
                        applied_damage = attack[weapon] * 2
                        self.current_hull_points -= applied_damage
                    elif self.armor_type == 1:
                        applied_damage = int(round((attack[weapon] - (attack[weapon] * .20)) * 2))
                        self.current_hull_points -= applied_damage
                    elif self.armor_type == 2:
                        applied_damage = int(round((attack[weapon] - (attack[weapon] * .30)) * 2))
                        self.current_hull_points -= applied_damage
                    elif self.armor_type == 3:
                        applied_damage = int(round((attack[weapon] - (attack[weapon] * .40)) * 2))
                        self.current_hull_points -= applied_damage
                    if self.current_hull_points > 0:
                        print("{} took {} damage!  {}'s current hull integrity is at {}."
                              .format(self.name, applied_damage, self.name, self.current_hull_points))
                        return False
                    else:
                        print("{} took {} damage!  {}'s ship has been destroyed!"
                              .format(self.name, applied_damage, self.name))
                        return True
                else:
                    applied_damage = int(round(attack[weapon] / 3))
                    self.current_shield_points -= applied_damage
                    if self.current_shield_points <= 0:
                        self.shields = False
                        print("{} took {} damage!  {}'s shield has been destroyed!"
                              .format(self.name, applied_damage, self.name))
                        return False
                    else:
                        print("{} took {} damage!  {}'s current shield strength is at {}."
                              .format(self.name, applied_damage, self.name, self.current_shield_points))
                        return False

    def upgrade(self):
        '''
        Need to put together how upgrades will work.  Either experience or currency.  I'm thinking probably currency is
        the best way to handle this.
        '''

        return False